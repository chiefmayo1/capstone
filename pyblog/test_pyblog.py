#!/usr/bin/env python
import unittest
import pyblog
import unittest.mock
import json


class MockResponse:

    def __init__(self, text, status_code):
        self.text = text
        self.status_code = status_code

    def json(self):
        return json.loads(self.text)

    def __iter__(self):
        return self

    def __next__(self):
        return self


def mock_requests_get_post(*args, **kwargs):
    text = """
        [{"id":21,
        "date":"2020-03-05T16:09:16",
        "date_gmt":"2020-03-05T16:09:16",
        "modified":"2020-03-05T16:09:16",
        "modified_gmt":"2020-03-05T16:09:16",
        "slug":"this-is-my-title-2",
        "status":"publish",
        "type":"post",
        "title":{"rendered":"This is my title."},
        "content":{"rendered":"Content."}
        }]
    """

    response = MockResponse(text, 200)
    return response


class WordPressTest(unittest.TestCase):

    @unittest.mock.patch('requests.request', mock_requests_get_post)
    def test_get_post(self):
        data = pyblog.get_post('', '')
        self.assertTrue(data)
        self.assertEqual('This is my title.', data[0])
        self.assertEqual('Content.', data[1])

    def test_cli_upload(self):
        file = open('latin')
        title = file.readline()
        content = file.readlines()
        querystring = pyblog.cli_upload('latin')
        self.assertTrue(file)
        self.assertEqual(title, querystring['title'])
        self.assertEqual(content, querystring['content'])

    def test_write_post(self):
        expected = '<Response [201]>'
        querystring = {"title": 'title',
                       "content": 'content',
                       "status": "publish"}
        result = pyblog.write_post(querystring,
                                   pyblog.read_userpass(),
                                   url='http://3.15.140.24:8088/wp-json/wp/v2/posts') # noqa
        self.assertEqual(expected, str(result))


if __name__ == '__main__':
    unittest.main()
