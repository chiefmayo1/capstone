#!/usr/bin/env python

import requests
import os
import base64
from sys import argv


def read_userpass():
    username = os.environ.get('WORDPRESS_USERNAME')
    password = os.environ.get('WORDPRESS_PASSWORD')
    userpass_str = username + ':' + password
    userpass_bytes = userpass_str.encode('ascii')
    userpass_64 = base64.b64encode(userpass_bytes)
    userpass = userpass_64.decode('utf-8')
    return userpass


def read_url():
    url = os.environ.get('WORDPRESS_URL')
    return url


# def cli_interactive():
#     create_post = input('Would you like to create a post? yes or no: ')
#     if create_post == 'yes':
#         post_file = input('Do you have your post in a file?  yes or no: ')
#         if post_file == 'yes':
#             cli_upload(post_file)
#         else:
#             title = input('Enter a post title: ')
#             content = input('Enter post content here: ')
#             status = input('draft or publish?: ')
#             querystring = {"title": title,
#                            "content": content,
#                            "status": status}
#         write_post(querystring, read_userpass(), read_url())
#     else:
#         view_post = input('''Enter view mode.
#                           How many posts would you
#                           like to view?: ''')
#         querystring = {"per_page": view_post}
#         get_post(read_userpass(), read_url())


def cli_upload(post_file):
    post = open(post_file)
    title = post.readline()
    content = post.readlines()
    querystring = {"title": title,
                   "content": content,
                   "status": "publish"}
    write_post(querystring, read_userpass(), read_url())
    return querystring


def get_post(authorization, url):
    querystring = {"per_page": 1}
    headers = {'Authorization': f"Basic {authorization}"}
    response = requests.request("GET",
                                url,
                                headers=headers,
                                params=querystring)
    title = response.json()[0]['title']['rendered']
    content = response.json()[0]['content']['rendered']
    return title, content


def write_post(querystring, authorization, url):
    headers = {'Authorization': f"Basic {authorization}"}
    response = requests.request("POST",
                                url,
                                headers=headers,
                                params=querystring)
    return response


if __name__ == '__main__':
    action = argv[1].lower()
    if action == 'read':
        title, content = get_post(read_userpass(), read_url())
        print(f'TITLE: {title}', f'CONTENT: {content}', sep="\n")
    elif action == 'upload':
        filename = argv[3]
        cli_upload(filename)
    else:
        print('WRONG! TRY AGAIN! SPECIFY READ OR UPLOAD')
